﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;

namespace RedbubbleImageScraper
{
    class Program
    {
        private static HttpClient httpClient = new HttpClient(new HttpClientHandler()
        {
            UseCookies = true,
            CookieContainer = new CookieContainer(),
            AutomaticDecompression = DecompressionMethods.GZip,
        })
        {
            BaseAddress = new Uri("https://www.redbubble.com"),
        };


        static async Task Main(string[] args)
        {
            httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.107");

            var getProductsBlock = new TransformBlock<string, List<Product>>(s => GetProducts(s));
            var getImageBlock = new TransformBlock<Product,string>(GetProductImageLink,new ExecutionDataflowBlockOptions(){MaxDegreeOfParallelism = 10});

            // Linking Blocks
            var linkOptions = new DataflowLinkOptions {PropagateCompletion = true};
            // Staring work
            getProductsBlock.Post("/shop/jdm+stickers?ref=search_box");
            getProductsBlock.Complete();
            getProductsBlock.Completion.Wait();
            
            List<Product> links;
            getProductsBlock.TryReceive(out links);

            foreach (var link in links)
            {
                getImageBlock.Post(link);
            }
            
            getImageBlock.Complete();
            getImageBlock.Completion.Wait();
        }

        private static async Task<string> GetProductImageLink(Product product)
        {
            Console.WriteLine($"Getting {product.Name} Image.");
            var link = product.Link;
            var result = await httpClient.GetStringAsync(link);
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(result);

            var jsonData = htmlDoc.DocumentNode.SelectSingleNode("//script[@data-product-data-json]")
                .GetAttributeValue("data-product-data-json", "");
            jsonData = HttpUtility.HtmlDecode(jsonData);
            var mainJObject = JObject.Parse(jsonData);
            var url = mainJObject["product_zoom_image_url"].Value<string>();
            Console.WriteLine(url);
            return url;
        }

        private static async Task<List<Product>> GetProducts(string path)
        {
            var htmlAsString = await httpClient.GetStringAsync(path);
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlAsString);
            htmlDoc.Save("index.html");
            var jsonData = htmlDoc.DocumentNode.SelectSingleNode("//script[contains(text(),'window.__APOLLO_STATE__')]")
                .InnerText;
            jsonData = jsonData.Replace("window.__APOLLO_STATE__=", "");
            jsonData = jsonData.Substring(0, jsonData.Length - 1);

            var mainJObject = JObject.Parse(jsonData);
            var searchResults = mainJObject["ShopSearchResultsjdm stickers"]["products"].Values("id").ToList();
            var products = new List<Product>();
            foreach (var searchResult in searchResults)
            {
                var productKey = searchResult.Value<string>();
                var productName = mainJObject[productKey]["fullTitle"].Value<string>();
                var productUrl = mainJObject[productKey]["productUrl"].Value<string>();
                products.Add(new Product
                {
                    Link = productUrl,
                    Name = productName
                });
            }

            return products;
        }
    }
}