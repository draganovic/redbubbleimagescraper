namespace RedbubbleImageScraper
{
    public class Product
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
}